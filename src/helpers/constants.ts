export const APIKEY = 'e92fdc433559d6b3161ce3114c29cccb';
export const TS = '1';
export const HASH = '72d087c06c28d9d8878eafade6e0737c';
export const BASEURL = 'https://gateway.marvel.com:443/v1/public';

type endpoint = 'comics' | 'series' | 'characters' | 'stories';

export const getURL = (whatToGet: endpoint) => {
  return `${BASEURL}/${whatToGet}?ts=${TS}&apikey=${APIKEY}&hash=${HASH}`;
};
export const searchById = (whatToGet: endpoint, id: string) => {
  return `${BASEURL}/${whatToGet}/${id}?ts=${TS}&apikey=${APIKEY}&hash=${HASH}`;
};

export const addFinalPart = (uri: string) => {
  return `${uri}?ts=${TS}&apikey=${APIKEY}&hash=${HASH}`;
};

export const ellipsify = (str: string) => {
  if (str.length > 10) {
    return str.substring(0, 28) + '...';
  } else {
    return str;
  }
};

export const SERIES = getURL('series');
export const COMICS = getURL('comics');
export const CHARACTERS = getURL('characters');
export const STORIES = getURL('stories');
export const STORYIMAGE =
  'https://i0.wp.com/books.disney.com/content/uploads/2020/07/1368054943-scaled.jpg?fit=2428%2C2560&ssl=1';

export const actionTypes = {
  SET_CHARACTERS: 'SET_CHARACTERS',
  SET_CHARACTER: 'SET_CHARACTER',
  SET_COMICS_RELATED: 'SET_COMICS_RELATED',
  SET_COMICS: 'SET_COMICS',
  SET_COMIC: 'SET_COMIC',
  SET_CHARACTER_RELATED: 'SET_CHARACTER_RELATED',
  SET_SERIES: 'SET_SERIES',
  SET_SERIE: 'SET_SERIE',
  SET_SERIES_RELATED: 'SET_SERIES_RELATED',
  SET_STORIES: 'SET_STORIES',
  SET_STORY: 'SET_STORY',
  SET_STORIES_RELATED: 'SET_STORIES_RELATED',
};

export type ActionTypes =
  | 'SET_CHARACTERS'
  | 'SET_CHARACTER'
  | 'SET_CHARACTER_RELATED'
  | 'SET_COMICS_RELATED'
  | 'SET_COMICS'
  | 'SET_COMIC'
  | 'SET_BOOKMARKS'
  | 'SET_BOOKMARK'
  | 'REMOVE_BOOKMARKS'
  | 'REMOVE_BOOKMARK';
