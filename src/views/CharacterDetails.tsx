import axios, { AxiosResponse } from 'axios';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { SingleCharacter } from '../components/characters/SingleCharacter';
import { searchById } from '../helpers/constants';
import { CharactersResponse } from '../interfaces/Character';
import { stateInterface } from '../interfaces/Share';
import { setCharacter } from '../redux/actions/charactersAction';

export const CharacterDetails = () => {
  useEffect(() => {
    getCharacter();
  }, []);

  const response = useSelector(
    (state: stateInterface) => state.allCharacters.character
  );

  const character = response;
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const getCharacter = async () => {
    const item = localStorage.getItem('hidden');
    const hidden = JSON.parse(item ? item : JSON.stringify([]));

    if (id && hidden.includes(parseInt(id))) {
      navigate('/error');
    }

    const result: void | AxiosResponse<CharactersResponse> = await axios
      .get(searchById('characters', id ? id : '0'))
      .catch((err) => {
        console.log(err);
      });
    if (result) {
      dispatch(setCharacter(result.data.data.results[0]));
    }
  };

  return <div>{character && <SingleCharacter character={character} />}</div>;
};
