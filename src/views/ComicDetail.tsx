import axios, { AxiosResponse } from 'axios';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { stateInterface } from '../interfaces/Share';
import { ComicsResponse } from '../interfaces/Comics';
import { searchById } from '../helpers/constants';
import { setComic } from '../redux/actions/comicsActions';
import { SingleComic } from '../components/comics/SingleComic';

export const ComicDetail = () => {
  useEffect(() => {
    getComic();
  }, []);
  const response = useSelector(
    (state: stateInterface) => state.allComics.comic
  );

  const comic = response;
  const { id } = useParams();

  const dispatch = useDispatch();
  const getComic = async () => {
    const result: void | AxiosResponse<ComicsResponse> = await axios
      .get(searchById('comics', id ? id : '0'))
      .catch((err) => {
        console.log(err);
      });
    if (result) {
      dispatch(setComic(result.data.data.results[0]));
    }
  };

  return <div>{comic && <SingleComic comic={comic} />}</div>;
};
