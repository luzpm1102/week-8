import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Bookmark, stateInterface } from '../interfaces/Share';
import { usePagination } from '../hooks/usePagination';
import { Card } from '../components/Card';
import { Loading } from '../components/Loading';
import Pagination from '../components/Pagination';
import { removeAll } from '../redux/actions/bookmarksAction';
import { ellipsify } from '../helpers/constants';

export const Bookmarks = () => {
  const bookmarks: Bookmark[] = useSelector(
    (state: stateInterface) => state.allBookmarks.bookmarks
  );
  const itemsLimit = 3;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = bookmarks
    ? bookmarks.length > 0
      ? bookmarks.slice(indexOfTheFirstItem, indexOfTheLastItem).map((item) => (
          <Link
            key={item.id}
            className='comics-list__item'
            to={`/${item.type}/${item.id}`}
          >
            <Card
              img={item.img}
              title={item.title ? ellipsify(item.title) : item.title}
            />
          </Link>
        ))
      : []
    : [];
  const dispatch = useDispatch();
  const deleteAll = () => {
    dispatch(removeAll());
  };

  return (
    <div>
      <h2 className='center'>bookmarks</h2>
      <div>
        {bookmarks && bookmarks.length <= 0 ? (
          <h3 className='center'>NO BOOKMARKS ADDED YET</h3>
        ) : (
          <>
            <div className='character-list'>{display}</div>
            <Pagination
              itemsPerPage={itemsPerPage}
              totalpages={bookmarks.length}
              paginate={paginate}
              currentPage={currentPage}
            />
            <div className='center'>
              <button className='home-button' onClick={deleteAll}>
                DELETE ALL
              </button>
            </div>
          </>
        )}
      </div>
    </div>
  );
};
