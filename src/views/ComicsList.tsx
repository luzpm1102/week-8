import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card } from '../components/Card';
import Filter from '../components/comics/Filter';
import { Loading } from '../components/Loading';
import Pagination from '../components/Pagination';
import { COMICS } from '../helpers/constants';
import { usePagination } from '../hooks/usePagination';
import { ComicsResponse } from '../interfaces/Comics';
import { stateInterface } from '../interfaces/Share';
import { setComics } from '../redux/actions/comicsActions';

export const ComicsList = () => {
  const [error, setError] = useState(false);
  const [update, setUpdate] = useState(false);
  const comics = useSelector((state: stateInterface) => state.allComics.comics);

  const dispatch = useDispatch();

  const recoverHidden = () => {
    localStorage.removeItem('hidden');
    setUpdate(true);
  };
  const getComics = async (query?: string) => {
    const result: void | AxiosResponse<ComicsResponse> = await axios
      .get(query ? COMICS + query : COMICS)
      .catch((err) => {
        setError(true);
      });
    if (result) {
      if (result.data.data.total === 0) {
        setError(true);
      } else {
        setError(false);
      }
      dispatch(setComics(result.data.data.results));
    }
  };

  useEffect(() => {
    getComics();
    setError(false);
    setUpdate(false);
  }, [update]);

  const itemsLimit = 6;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = comics
    .slice(indexOfTheFirstItem, indexOfTheLastItem)
    .map((comic) => (
      <Link
        key={comic.id}
        className='comics-list__item'
        to={`/comic/${comic.id}`}
      >
        <Card
          img={`${comic.thumbnail.path}.${comic.thumbnail.extension}`}
          title={comic.title}
        />
      </Link>
    ));

  return (
    <>
      <Filter getComics={getComics} />
      {error && <h3 className='error'>NOT FOUND</h3>}
      <div className='center'>
        <button className='home-button' onClick={recoverHidden}>
          RECOVER HIDDEN
        </button>
      </div>
      {comics.length <= 0 && !error && <Loading />}
      <div className='character-list'>{display}</div>
      <Pagination
        itemsPerPage={itemsPerPage}
        totalpages={comics.length}
        paginate={paginate}
        currentPage={currentPage}
      />
    </>
  );
};
