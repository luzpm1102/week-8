import '../styles/character.scss';
import '../styles/general.scss';
import { useNavigate } from 'react-router-dom';
const logo = require('../assets/marvel.webp');
export const Home = () => {
  const navigate = useNavigate();
  const onClick = () => window.open('https://marvel.com', '_blank');
  return (
    <div className='home-container'>
      <div className='hero-container'></div>
      <div className='center'>
        <div className='home-info'>
          <h2 className='home__title'>MARVEL INFORMATION</h2>
          <p className='home__p'>
            Marvel Entertainment, LLC, a wholly-owned subsidiary of The Walt
            Disney Company, is one of the world's most prominent character-based
            entertainment companies, built on a proven library of more than
            8,000 characters featured in a variety of media over seventy-five
            years. Marvel utilizes its character franchises in entertainment,
            licensing and publishing. For more information visit marvel.com.
          </p>
          <small> © 2020 MARVEL</small>
          <button className='home-button' onClick={onClick}>
            LEARN MORE
          </button>
        </div>
      </div>
    </div>
  );
};
