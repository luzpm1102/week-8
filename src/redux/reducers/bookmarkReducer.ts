import { actionTypes, ActionTypes } from '../../helpers/constants';
import { Bookmark } from '../../interfaces/Share';

// type bookmarkAtionType = {
//   type: ActionTypes;
//   payload: | Bookmark | number;
// };
type bookmarkAtionType =
  | { type: 'SET_BOOKMARK'; payload: Bookmark }
  | { type: 'REMOVE_BOOKMARK'; payload: number }
  | { type: 'REMOVE_BOOKMARKS'; payload: null };
type initialStateType = {
  bookmarks: Bookmark[];
};
const initialState: initialStateType = {
  bookmarks: [],
};

export const bookmarkReducer = (
  state = initialState,
  { type, payload }: bookmarkAtionType
) => {
  switch (type) {
    case 'SET_BOOKMARK':
      const dontAddTheSame = state.bookmarks.filter(
        (bookmark) => bookmark.id === payload.id
      );

      if (dontAddTheSame.length > 0) {
        return { ...state };
      }
      return { ...state, bookmarks: [...state.bookmarks, payload] };

    case 'REMOVE_BOOKMARKS':
      return { ...state, bookmarks: [] };

    case 'REMOVE_BOOKMARK':
      const newPayload = state.bookmarks.filter(
        (bookmark) => bookmark.id !== payload
      );

      return { ...state, bookmarks: newPayload };
    default:
      return state;
  }
};
