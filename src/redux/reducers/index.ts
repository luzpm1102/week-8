import { combineReducers } from 'redux';
import { characterReducer } from './characterReducer';
import { comicsReducer } from './comicsReducer';
import { seriesReducer } from './seriesReducer';
import { storiesReducer } from './storiesReducer';
import { bookmarkReducer } from './bookmarkReducer';

export const reducers = combineReducers({
  allCharacters: characterReducer,
  allComics: comicsReducer,
  allSeries: seriesReducer,
  allStories: storiesReducer,
  allBookmarks: bookmarkReducer,
});
