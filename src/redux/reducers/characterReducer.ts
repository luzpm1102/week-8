import { Character } from '../../interfaces/Character';
import { actionTypes, ActionTypes } from '../../helpers/constants';

const initialState = {
  characters: [],
};

type characterActionType = {
  type: ActionTypes;
  payload: Character[];
};

export const characterReducer = (
  state = initialState,
  { type, payload }: characterActionType
) => {
  switch (type) {
    case actionTypes.SET_CHARACTERS:
      const item = localStorage.getItem('hidden');

      const hidden: number[] = JSON.parse(item ? item : JSON.stringify([]));

      if (hidden.length > 0) {
        let newPayload = payload.filter(
          (character) => !hidden.includes(character.id)
        );

        return { ...state, characters: newPayload };
      }
      return { ...state, characters: payload };

    case actionTypes.SET_CHARACTER:
      return { ...state, character: payload };

    case actionTypes.SET_CHARACTER_RELATED:
      return { ...state, characterRelated: payload };

    default:
      return state;
  }
};
