import { actionTypes } from '../../helpers/constants';
import { Serie } from '../../interfaces/Series';

export const setSeriesRelated = (series: Serie[]) => {
  return {
    type: actionTypes.SET_SERIES_RELATED,
    payload: series,
  };
};
export const setSeries = (series: Serie[]) => {
  return {
    type: actionTypes.SET_SERIES,
    payload: series,
  };
};
export const setSerie = (serie: Serie) => {
  return {
    type: actionTypes.SET_SERIE,
    payload: serie,
  };
};
