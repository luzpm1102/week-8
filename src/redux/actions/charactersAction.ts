import { actionTypes } from '../../helpers/constants';
import { Character } from '../../interfaces/Character';
export const setCharacters = (characters: Character[]) => {
  return {
    type: actionTypes.SET_CHARACTERS,
    payload: characters,
  };
};
export const setCharacter = (character: Character) => {
  return {
    type: actionTypes.SET_CHARACTER,
    payload: character,
  };
};
export const setCharactersRelated = (characters: Character[]) => {
  return {
    type: actionTypes.SET_CHARACTER_RELATED,
    payload: characters,
  };
};
