import { Comic } from '../../interfaces/Comics';
import { actionTypes } from '../../helpers/constants';

export const setComicsRelated = (comics: Comic[]) => {
  return {
    type: actionTypes.SET_COMICS_RELATED,
    payload: comics,
  };
};
export const setComics = (comics: Comic[]) => {
  return {
    type: actionTypes.SET_COMICS,
    payload: comics,
  };
};
export const setComic = (comic: Comic) => {
  return {
    type: actionTypes.SET_COMIC,
    payload: comic,
  };
};
