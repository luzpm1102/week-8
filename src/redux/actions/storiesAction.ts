import { actionTypes } from '../../helpers/constants';
import { Story } from '../../interfaces/Stories';

export const setStoriesRelated = (stories: Story[]) => {
  return {
    type: actionTypes.SET_STORIES_RELATED,
    payload: stories,
  };
};
export const setStories = (stories: Story[]) => {
  return {
    type: actionTypes.SET_STORIES,
    payload: stories,
  };
};
export const setStory = (story: Story) => {
  return {
    type: actionTypes.SET_STORY,
    payload: story,
  };
};
