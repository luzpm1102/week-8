import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stateInterface } from '../../interfaces/Share';
import { ComicsResponse } from '../../interfaces/Comics';
import { setComicsRelated } from '../../redux/actions/comicsActions';
import { Card } from '../Card';
import { Loading } from '../Loading';
import { usePagination } from '../../hooks/usePagination';
import Pagination from '../Pagination';
import { Link } from 'react-router-dom';
interface Props {
  uri: string;
}

export const ComicsRelated = ({ uri }: Props) => {
  const comicsRelated = useSelector(
    (state: stateInterface) => state.allComics.comicsRelated
  );
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const getComics = async () => {
    const result: void | AxiosResponse<ComicsResponse> = await axios
      .get(uri)
      .catch((err) => console.log(err));
    if (result) {
      dispatch(setComicsRelated(result.data.data.results));
      setLoading(false);
    }
  };

  useEffect(() => {
    getComics();
  }, []);

  const itemsLimit = 3;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = comicsRelated
    ? comicsRelated
        .slice(indexOfTheFirstItem, indexOfTheLastItem)
        .map((comic) => (
          <Link to={`/comic/${comic.id}`} key={comic.id}>
            <Card
              img={`${comic.thumbnail.path}.${comic.thumbnail.extension}`}
              title={comic.title}
            />
          </Link>
        ))
    : [];
  return (
    <>
      <h3 className='center'>Comics related</h3>
      {loading && <Loading />}

      <div className='related-container'>
        {comicsRelated && comicsRelated.length > 0 && display}
      </div>
      <>
        {comicsRelated && comicsRelated.length <= 0 ? (
          <h4>No comics related</h4>
        ) : (
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={comicsRelated ? comicsRelated.length : 0}
            paginate={paginate}
            currentPage={currentPage}
          />
        )}
      </>
    </>
  );
};
