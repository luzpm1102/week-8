import React, { useEffect, useState } from 'react';
import { Comic } from '../../interfaces/Comics';
import { addFinalPart } from '../../helpers/constants';
import { CharacterRelated } from '../characters/CharacterRelated';
import { StoriesRelated } from '../stories/storiesRelated';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Bookmark, stateInterface } from '../../interfaces/Share';
import { removeOne, setBookmark } from '../../redux/actions/bookmarksAction';

interface Props {
  comic: Comic;
}
export const SingleComic = ({ comic }: Props) => {
  const { thumbnail, description, title, characters, stories, id } = comic;
  const characterRelatedURI = addFinalPart(characters.collectionURI);
  const storyRelatedURI = addFinalPart(stories.collectionURI);
  const [favorite, setFavorite] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const hide = () => {
    const item = localStorage.getItem('hidden');
    const hidden: number[] = JSON.parse(item ? item : JSON.stringify([]));
    const copy = [...hidden, id];
    localStorage.setItem('hidden', JSON.stringify(copy));
    navigate('/comics');
  };

  useEffect(() => {
    if (bookmarks.includes(id)) {
      setFavorite(true);
    }
  }, []);

  const bookmarks = useSelector((state: stateInterface) =>
    state.allBookmarks.bookmarks.map((item) => item.id)
  );

  const deleteBookmark = () => {
    setFavorite(false);
    dispatch(removeOne(id));
  };

  const addToBookmark = () => {
    const bookmark: Bookmark = {
      id: id,
      type: 'comic',
      img: `${thumbnail.path}.${thumbnail.extension}`,
      title: title,
    };
    setFavorite(true);
    dispatch(setBookmark(bookmark));
  };

  return (
    <div className='flex-container'>
      <div className='character-info'>
        <div className='image-container'>
          <img
            src={`${thumbnail.path}.${thumbnail.extension}`}
            alt={thumbnail.path}
            className='character__img'
          />
        </div>
        <div className='info-container'>
          <p className='info__item__title'> {title.toUpperCase()}</p>
          <h3 className='info__item'>Description:</h3>
          <p>{description ? description : 'No description'}</p>
          <div className='buttons-container'>
            <button onClick={hide} className='home-button'>
              HIDE
            </button>
            <button
              onClick={favorite ? deleteBookmark : addToBookmark}
              className='home-button'
            >
              {favorite ? 'REMOVE' : 'SAVE'}
            </button>
          </div>
        </div>
      </div>
      <CharacterRelated uri={characterRelatedURI} />
      <StoriesRelated uri={storyRelatedURI} />
    </div>
  );
};
