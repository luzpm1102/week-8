import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stateInterface } from '../../interfaces/Share';
import { Card } from '../Card';
import { CharactersResponse } from '../../interfaces/Character';
import { setCharactersRelated } from '../../redux/actions/charactersAction';
import { Loading } from '../Loading';
import { usePagination } from '../../hooks/usePagination';
import Pagination from '../Pagination';
import { Link } from 'react-router-dom';
interface Props {
  uri: string;
}

export const CharacterRelated = ({ uri }: Props) => {
  const [loading, setLoading] = useState(true);
  const characters = useSelector(
    (state: stateInterface) => state.allCharacters.characterRelated
  );
  const dispatch = useDispatch();

  const getCharacterRelated = async () => {
    const result: void | AxiosResponse<CharactersResponse> = await axios
      .get(uri)
      .catch((err) => console.log(err));
    if (result) {
      dispatch(setCharactersRelated(result.data.data.results));
      setLoading(false);
    }
  };

  useEffect(() => {
    getCharacterRelated();
  }, []);

  const itemsLimit = 3;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = characters
    ? characters
        .slice(indexOfTheFirstItem, indexOfTheLastItem)
        .map((character) => (
          <Link to={`/character/${character.id}`} key={character.id}>
            <Card
              key={character.id}
              img={`${character.thumbnail.path}.${character.thumbnail.extension}`}
              title={character.name}
            />
          </Link>
        ))
    : [];
  return (
    <>
      <h3 className='center'>Characters related</h3>
      {loading && <Loading />}

      <div className='related-container'>
        {characters && characters.length > 0 && display}
      </div>
      <>
        {characters && characters.length <= 0 ? (
          <h4>No characters related</h4>
        ) : (
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={characters ? characters.length : 0}
            paginate={paginate}
            currentPage={currentPage}
          />
        )}
      </>
    </>
  );
};
