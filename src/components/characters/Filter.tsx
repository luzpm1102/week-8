import React, { useEffect, useState } from 'react';
import { debounce } from 'lodash';
import { useNavigate } from 'react-router-dom';
import '../../styles/filter.scss';
import { useSelector } from 'react-redux';
import { stateInterface } from '../../interfaces/Share';
import { useHelp } from '../../hooks/useHelp';

interface Props {
  getCharacters: (query?: string) => Promise<void>;
}

const Filter = ({ getCharacters }: Props) => {
  const { getComics, getStories } = useHelp();

  useEffect(() => {
    getComics();
    getStories();
  }, []);

  const url = new URL(window.location.href);
  const [query, setQuery] = useState<string>('');
  const navigate = useNavigate();
  const params = url.search;
  const comics = useSelector((state: stateInterface) => state.allComics.comics);
  const stories = useSelector(
    (state: stateInterface) => state.allStories.stories
  );

  const setName = (value: string) => {
    url.searchParams.set('nameStartsWith', value);
    navigate(`${url.search}`);
  };
  const getName = debounce(setName, 900);

  const comic = (value: string) => {
    url.searchParams.set('comics', value);
    navigate(`${url.search}`);
  };

  const storie = (value: string) => {
    url.searchParams.set('stories', value);
    navigate(`${url.search}`);
  };

  if (params) {
    const name = url.searchParams.get('nameStartsWith');
    const comics = url.searchParams.get('comics');
    const stories = url.searchParams.get('stories');
    const searchQuery = `${name ? `&nameStartsWith=${name}` : ''}${
      comics ? `&comics=${comics}` : ''
    }${stories ? `&stories=${stories}` : ''}`;
    if (query !== searchQuery) {
      getCharacters(searchQuery);
      setQuery(searchQuery);
    }
  }

  return (
    <div className='filter'>
      <div className='filter-box'>
        <label htmlFor='search' className='filter__label'>
          Name
        </label>
        <input
          className='filter__input'
          type='text'
          name='search'
          id='search'
          placeholder='Characters Name'
          onChange={(e) => getName(e.target.value)}
        />
      </div>
      <div className='filter-box'>
        <label htmlFor='comic' className='filter__label'>
          Comic
        </label>
        <select
          className='filter__input'
          name='comic'
          id='comic'
          onChange={(e) => comic(e.target.value)}
        >
          <option value=''>All</option>
          {comics?.map((comic) => (
            <option key={comic.id} value={comic.id}>
              {comic.title}
            </option>
          ))}
        </select>
      </div>
      <div className='filter-box'>
        <label htmlFor='stories' className='filter__label'>
          Stories
        </label>
        <select
          className='filter__input'
          name='stories'
          id='stories'
          onChange={(e) => storie(e.target.value)}
        >
          <option value=''>All</option>
          {stories?.map((story) => (
            <option key={story.id} value={story.id}>
              {story.title}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default Filter;
