import React from 'react';
import '../styles/card.scss';
interface Props {
  img: string;
  title: string;
}

export const Card = ({ img, title }: Props) => {
  return (
    <div className='card'>
      <div className='card__img-container'>
        <img className='card__img' src={img} alt={img} />
      </div>
      <div className='card__title-container'>
        <h4 className='card__title'>{title}</h4>
      </div>
    </div>
  );
};
