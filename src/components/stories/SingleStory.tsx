import React, { useEffect, useState } from 'react';
import { addFinalPart, STORYIMAGE } from '../../helpers/constants';
import { CharacterRelated } from '../characters/CharacterRelated';

import { ComicsRelated } from '../comics/ComicsRelated';
import { Story } from '../../interfaces/Stories';
import { Bookmark, stateInterface } from '../../interfaces/Share';
import { type } from '@testing-library/user-event/dist/type';
import { useDispatch, useSelector } from 'react-redux';
import { removeOne, setBookmark } from '../../redux/actions/bookmarksAction';
import { useNavigate } from 'react-router-dom';

interface Props {
  story: Story;
}
export const SingleStory = ({ story }: Props) => {
  const { description, title, characters, comics, id } = story;
  const characterRelatedURI = addFinalPart(characters.collectionURI);
  const comicsRelatedURI = addFinalPart(comics.collectionURI);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [favorite, setFavorite] = useState(false);
  const hide = () => {
    const item = localStorage.getItem('hidden');
    const hidden: number[] = JSON.parse(item ? item : JSON.stringify([]));
    const copy = [...hidden, id];
    localStorage.setItem('hidden', JSON.stringify(copy));
    navigate('/stories');
  };
  useEffect(() => {
    if (bookmarks.includes(id)) {
      setFavorite(true);
    }
  }, []);

  const bookmarks = useSelector((state: stateInterface) =>
    state.allBookmarks.bookmarks.map((item) => item.id)
  );

  const deleteBookmark = () => {
    setFavorite(false);
    dispatch(removeOne(id));
  };

  const addToBookmark = () => {
    const bookmark: Bookmark = {
      id: id,
      type: 'story',
      img: STORYIMAGE,
      title: title,
    };
    setFavorite(true);
    dispatch(setBookmark(bookmark));
  };
  return (
    <div className='flex-container'>
      <div className='character-info'>
        <div className='image-container'>
          <img src={STORYIMAGE} className='character__img' />
        </div>
        <div className='info-container'>
          <p className='info__item__title-story'> {title.toUpperCase()}</p>
          <h3 className='info__item'>Description:</h3>
          <p>{description ? description : 'No description'}</p>
          <div className='buttons-container'>
            <button onClick={hide} className='home-button'>
              HIDE
            </button>
            <button
              onClick={favorite ? deleteBookmark : addToBookmark}
              className='home-button'
            >
              {favorite ? 'REMOVE' : 'SAVE'}
            </button>
          </div>
        </div>
      </div>
      <CharacterRelated uri={characterRelatedURI} />
      <ComicsRelated uri={comicsRelatedURI} />
    </div>
  );
};
