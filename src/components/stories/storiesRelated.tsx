import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stateInterface } from '../../interfaces/Share';
import { Card } from '../Card';
import { Loading } from '../Loading';
import { StoriesResponse } from '../../interfaces/Stories';
import { setStoriesRelated } from '../../redux/actions/storiesAction';
import { STORYIMAGE } from '../../helpers/constants';
import { usePagination } from '../../hooks/usePagination';
import Pagination from '../Pagination';
import { Link } from 'react-router-dom';
interface Props {
  uri: string;
}

export const StoriesRelated = ({ uri }: Props) => {
  const storiesRelated = useSelector(
    (state: stateInterface) => state.allStories.storiesRelated
  );
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const getStories = async () => {
    const result: void | AxiosResponse<StoriesResponse> = await axios
      .get(uri)
      .catch((err) => console.log(err));
    if (result) {
      dispatch(setStoriesRelated(result.data.data.results));
      setLoading(false);
    }
  };

  useEffect(() => {
    getStories();
  }, []);

  const itemsLimit = 3;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = storiesRelated
    ? storiesRelated
        .slice(indexOfTheFirstItem, indexOfTheLastItem)
        .map((story) => (
          <Link key={story.id} to={`/story/${story.id}`}>
            <Card img={STORYIMAGE} title={story.title} />
          </Link>
        ))
    : [];

  return (
    <>
      <h3 className='center'>Stories related</h3>
      {loading && <Loading />}
      <div className='related-container'>
        {storiesRelated && storiesRelated.length > 0 && display}
      </div>

      <>
        {storiesRelated && storiesRelated.length <= 0 ? (
          <h4>No stories related</h4>
        ) : (
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={storiesRelated ? storiesRelated.length : 0}
            paginate={paginate}
            currentPage={currentPage}
          />
        )}
      </>
    </>
  );
};
