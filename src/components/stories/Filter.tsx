import React from 'react';
import { debounce } from 'lodash';
import { useNavigate } from 'react-router-dom';
interface Props {
  getSeries: (query?: string) => Promise<void>;
}

const Filter = () => {
  const url = new URL(window.location.href);
  const navigate = useNavigate();

  const setTitle = (value: string) => {
    url.searchParams.set('title', value);
    navigate(`${url.search}`);
  };

  const getTitle = debounce(setTitle, 900);

  return (
    <div className='filter'>
      <div>
        <label htmlFor='title' className='filter__label'>
          Title
          <input
            className='filter__input'
            type='text'
            name='title'
            id='title'
            placeholder='name'
            onChange={(e) => getTitle(e.target.value)}
          />
        </label>
      </div>
    </div>
  );
};

export default Filter;
