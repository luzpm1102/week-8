import { Link } from 'react-router-dom';
import { paths } from './Navigation';
import '../../styles/navbar.scss';
export const NavBar = () => {
  return (
    <nav className='nav-container'>
      <div className='nav-logo'>
        <Link to={paths.home} className='nav-logo'>
          <h4 className='nav-list__item--text logo'>InfoMarvel</h4>
        </Link>
      </div>
      <ul className='nav-list'>
        <li className='nav-list__item'>
          <Link className='nav-list__item--text' to={paths.characters}>
            Characters
          </Link>
        </li>
        <li className='nav-list__item'>
          <Link className='nav-list__item--text' to={paths.comics}>
            Comics
          </Link>
        </li>
        <li className='nav-list__item'>
          <Link className='nav-list__item--text' to={paths.stories}>
            Stories
          </Link>
        </li>
        <li className='nav-list__item'>
          <Link className='nav-list__item--text' to={paths.bookmarks}>
            Bookmarks
          </Link>
        </li>
      </ul>
    </nav>
  );
};
